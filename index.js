const axios = require('axios').default;
const fs = require('fs')
const path = require('path')
require('dotenv').config({ path: '/home/mik/projects/joegame-twitter-dialogue-scraper/.env' })

const bearerToken = process.env.TWITTER_BEARER_TOKEN
const getTweetEndpoint = (id) => `/2/tweets?ids=${id}&tweet.fields=author_id,conversation_id,created_at,in_reply_to_user_id,referenced_tweets&expansions=author_id,in_reply_to_user_id,referenced_tweets.id&user.fields=name,username`
const getUserEndpoint = (id) => `/2/users/${id}`
const inst = axios.create({
  baseURL: 'https://api.twitter.com/',
  headers: {
    Authorization: `Bearer ${bearerToken}`
  }
})

async function getUsername(rootID) {
  let res = await inst.get(getUserEndpoint(rootID))
  if (res.data.data) {
    return res.data.data.username
  } else {
    return ''
  }
}
async function getTweetAndNext(rootID) {
  const tweet = await inst.get(getTweetEndpoint(rootID))
  let response
  let inReplyTo = undefined
  if (tweet.data.data) {
    response = tweet.data.data[0]
    if (tweet.data.data[0].referenced_tweets) {
      inReplyTo = tweet.data.data[0].referenced_tweets.find((v) => v.type = 'replied_to')
    }
  } else {
    response = undefined
  }
  return [response, inReplyTo]
}

async function getConvo(startID) {
  let tweets = []
  let currID = startID
  let done = false
  while (done != true) {
    let tn = await getTweetAndNext(currID)

    if (tn[0]) {
      tn[0].username = await getUsername(tn[0].author_id)
      tweets.push(tn[0])
      if (tn[1]) {
        currID = tn[1].id
      } else {
        done = true
      }
    } else {
      done = true
    }
  }
  return tweets
}

(async function() {
  let manifest = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'convos/convo-manifest.json')))
  let argid = process.argv[2]
  if (argid === undefined) throw new Error('no arg id given!!!')
  let convo = await getConvo(argid)
  let filename = convo.length === 1 ? `${argid}_single.json` : `${argid}.json`
  manifest.push(filename)
  fs.writeFileSync(path.resolve(__dirname, `convos/${filename}`), JSON.stringify(convo))
  fs.writeFileSync(path.resolve(__dirname, `convos/convo-manifest.json`), JSON.stringify(manifest))
})()
