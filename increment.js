const Integer = require('integer')

const axios = require('axios').default;
require('dotenv').config({ path: '/home/mik/projects/joegame-twitter-dialogue-scraper/.env' })


const bearerToken = process.env.TWITTER_BEARER_TOKEN
const getTweetEndpoint = (id) => `/2/tweets?ids=${id}&tweet.fields=author_id,conversation_id,created_at,in_reply_to_user_id,referenced_tweets&expansions=author_id,in_reply_to_user_id,referenced_tweets.id&user.fields=name,username`

const inst = axios.create({
    baseURL: 'https://api.twitter.com/',
    headers: {
        Authorization: `Bearer ${bearerToken}`
    }
})

async function getNextTweet(rootID) {
    let next = rootID.add(1)

    let res = await inst.get(getTweetEndpoint(next.toString()))
    if (res.data.data) {
        console.log(res.data.data)
        // return res.data.data
    } else {
        console.log(next.toString())
        console.log('again!')
        getNextTweet(next)
    }
}

(async function() {
    let argid = process.argv[2]
    // if (argid === undefined) throw new Error('no arg id given!!!')
    let tweet = getNextTweet(new Integer.fromString(argid))
    // console.log(tweet)
    // console.log(typeof argid)
})()
