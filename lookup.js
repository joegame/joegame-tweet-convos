const axios = require('axios').default;
require('dotenv').config({ path: '/home/mik/projects/joegame-twitter-dialogue-scraper/.env' })


const bearerToken = process.env.TWITTER_BEARER_TOKEN
const getTweetEndpoint = (id) => `/2/tweets?ids=${id}&tweet.fields=author_id,conversation_id,created_at,in_reply_to_user_id,referenced_tweets&expansions=author_id,in_reply_to_user_id,referenced_tweets.id&user.fields=name,username`

const inst = axios.create({
    baseURL: 'https://api.twitter.com/',
    headers: {
        Authorization: `Bearer ${bearerToken}`
    }
})

async function getTweet(rootID) {
    let res = await inst.get(getTweetEndpoint(rootID))
    if (res.data.data) {
        return res.data.data
    } else {
        return ''
    }
}

(async function() {
    let argid = process.argv[2]
    if (argid === undefined) throw new Error('no arg id given!!!')
    let tweet = await getTweet(argid)
    console.log(tweet)
})()
